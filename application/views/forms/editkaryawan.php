
<body>
   <?php
  foreach ($detail_karyawan as $data) {
    $nik          = $data->nik;
    $nama_lengkap = $data->nama_lengkap;
    $tempat_lahir = $data->tempat_lahir;
    $tgl_lahir = $data->tgl_lahir;
    $jenis_kelamin = $data->jenis_kelamin;
    $alamat = $data->alamat;
    $telp = $data->telp;
    $kode_jabatan = $data->kode_jabatan;
    $foto   = $data->foto;


  }
  //pisah tanggal bulan tahun
  $thn_pisah = substr($tgl_lahir, 0, 4);
  $bln_pisah = substr($tgl_lahir, 5, 2);
  $tgl_pisah = substr($tgl_lahir, 8, 2);
  ?>

  <form action="<?=base_url()?>Karyawan/editkaryawan/<?= $nik; ?>" method="POST" enctype="multipart/form-data">
<table width="46%" border="0" cellspacing="0" cellpadding="5" bgcolor="green">

 

  

  <tr>
    <td width="43%">Nik</td>
    <td width="5%">:</td>
    <td width="52%">
      <input value="<?=$nik;?>" type="text" name="nik" id="nik" readonly/>
    </td>
  </tr>
  <tr>
    <td>Nama Karyawan</td>
    <td>:</td>
    <td>
      <input value="<?=$nama_lengkap;?>"  type="text" name="nama_karyawan" id="nama_karyawan" />
     </td>
</td>
  </tr>
 
  
  <tr>
    <td>Tempat Lahir</td>
    <td>:</td>
    <td>
      <input value="<?=$tempat_lahir; ?>" type="text" name="tempat_lahir" id="tempat_lahir" />
     </td>
</td>
  <tr>
    <td height="35">Jenis Kelamin</td>
    <td>:</td>
    <td><?php
      if($jenis_kelamin == 'P'){
       $slc_P = 'SELECTED';
       $slc_L = '';
    }elseif($jenis_kelamin == 'L'){
       $slc_L = 'SELECTED';
       $slc_P = '';
    }else{
       $slc_P = '';
       $slc_L = '';
    }
  ?>
    <label for="jenis_kelamin"></label>
      <select name="jenis_kelamin" id="jenis_kelamin">
      <option <?=$slc_P;?> value="P">Perempuan</option>
      <option <?=$slc_L;?> value="L">Laki-Laki</option>
      </select>
    </td>
  </tr>
 <tr>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>jQuery UI Datepicker - Default functionality</title>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <td>Tanggal Lahir</td>
        <td>:</td>
              <td>
                <script>
                  $(function(){
                    $("#tgl_lahir").datepicker({dateFormat: "yy-mm-dd"});
                  });
                </script>
                <input type="text" name="tgl_lahir" id="tgl_lahir" value="<?=set_value('tgl_lahir');?>">
              </td>
    </tr>


  <tr>
    <td>Upload Foto</td>
    <td>:</td>
    <td style="text-align: left;">
      <input type="file" name="image" id="image">
      <input type="text" name="foto_old" id="foto_old" value="<?=$foto; ?>">
      
    </td>
  </tr>
  
    <tr>
    <td>Telpon</td>
    <td>:</td>
    <td>
      <input value="<?=$telp; ?>" type="text" name="telpon" id="telpon" />
    </td>
  </tr>

 <tr>
    <td>Alamat</td>
    <td>:</td>
    <td>
      <textarea name="alamat" id="alamat" cols="45" rows="5"><?=$alamat; ?></textarea>
    </td>
  </tr>
  
  <tr>
    <td>Jabatan</td>
    <td>:</td>
    <td>
      
        <select id="kode_jabatan" name="kode_jabatan">
          <?php 
          foreach ($data_jabatan as $data) {
            $select_jabatan = ($data->kode_jabatan ==
              $kode_jabatan) ? 'selected' : '';
              ?>
            <option value="<?=$data->kode_jabatan; ?>" <?=
            $select_jabatan; ?>><?= $data->nama_jabatan; ?></option>
          <?php } ?>
          
        </select>
     
    </td>
  </tr>
 
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <input type="Submit" name="Submit" id="Submit" value="Simpan" />
      <input type="reset" name="reset" id="reset" value="Reset" />
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>
      <a href="<?= base_url(); ?>Karyawan/listkaryawan">
            <input type="button" value="kembali ke Menu Sebelumnya" name="kembali" />
      </a>
  </tr>
  
</table>
</form>

</body>
