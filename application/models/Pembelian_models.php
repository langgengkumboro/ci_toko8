<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian_models extends CI_Model
{
   //panggil nama table
    private $_table_header = "pembelian_header";
    private $_table_detail = "pembelian_detail";

    public function tampilDataPembelian()
        {
            $query  = $this->db->query(
                "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
            );
            return $query->result();  
        }

    public function savePembelianHeader()
        {
            $data['no_transaksi']   = $this->input->post('no_transaksi');
            $data['kode_supplier']  = $this->input->post('kode_supplier');
            $data['tanggal']        = date('Y-m-d');
            $data['approved']       = 1;
            $data['flag']           = 1;

            $this->db->insert($this->_table_header, $data);
        }
     public function idTransaksiTerakhir()
        {
            $query  = $this->db->query(
                "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_pembelian_h DESC LIMIT 0,1"
            );
            $data_id = $query->result();

            foreach ($data_id as $data) {
                $last_id = $data->id_pembelian_h;
            }

            return $last_id;
        }

    public function tampilDataPembelianDetail($id)
        {
            $query  = $this->db->query(
                "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_pembelian_h = '$id'"
            );
            return $query->result();    
        }

    public function savePembelianDetail($id)
        {
            $qty    = $this->input->post('qty');
            $harga  = $this->input->post('harga');

            $data['id_pembelian_h'] = $id;
            $data['kode_barang']    = $this->input->post('kode_barang');
            $data['qty']            = $qty;
            $data['harga']          = $harga;
            $data['jumlah']         = $qty * $harga;
            $data['flag']           = 1;

            $this->db->insert($this->_table_detail, $data);
        }
// public function tampillaporanpembelian($tgl_awal,$tgl_akhir)
public function tampillaporanpembelian($tgl_awal, $tgl_akhir)

    {
        // print_r($tgl_awal); die();
        // $this->db->select('barang.stock, pembelian_detail.qty, pembelian_detail.jumlah, pembelian_header.tanggal, pembelian_header.no_transaksi, pembelian_header.id_pembelian_h');
        // $this->db->from('pembelian_header');
        // $this->db->join('pembelian_detail', 'pembelian_detail.id_pembelian_h = pembelian_header.id_pembelian_h');
        // $this->db->join('barang', 'pembelian_detail.kode_barang = barang.kode_barang');
        // // $this->db->where('tanggal >=', $tgl_awal);
        // //  $this->db->where('tanggal <=', $tgl_akhir);
        // $this->db->group_by('pembelian_header.no_transaksi','asc');
        $this->db->select(' ph.id_pembelian_h, ph.no_transaksi,ph.tanggal, COUNT(pd.kode_barang) AS total_barang, SUM(pd.qty) as total_qty, SUM(pd.jumlah) as total_pembelian '); 
        $this->db->FROM ('pembelian_header ph');
        $this->db->JOIN ('pembelian_detail pd', 'ph.id_pembelian_h = pd.id_pembelian_h');
        $this->db->where("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
        $this->db->GROUP_BY('ph.no_transaksi');
        $query = $this->db->get();

        // SELECT ph.id_pembelian_h, ph.no_transaksi,ph.tanggal, COUNT(pd.kode_barang) AS kode_barang, SUM(pd.qty), SUM(pd.jumlah) as qty FROM pembelian_header AS ph INNER JOIN pembelian_detail as pd on ph.id_pembelian_h = pd.id_pembelian_h GROUP BY ph.no_transaksi ASC

             return $query->result();
          //  $_config[0] =& $config;  
          // return $_config[0];
    }

public function tampilDataPembelianPagination($perpage, $uri, $data_pencarian)
    {
        // echo "<pre>";
        // print_r($data_pencarian); die();
        // echo "</pre>";
        $this->db->select('*');
        if (!empty($data_pencarian)) {
            $this->db->like('no_transaksi', $data_pencarian);
        }
        $this->db->order_by('id_pembelian_h','asc');

        $get_data = $this->db->get($this->_table_header, $perpage, $uri);
        if ($get_data->num_rows() > 0) {
            return$get_data->result();
        }else{
            return null;
        }

    }

public function tombolpagination($data_pencarian)
    {
        //echo "<pre>";
        //print_r($data_pencarian); die();
        //echo "</pre>";
        //cari jmlh data berdasarkan data pencarian
        $this->db->like('no_transaksi', $data_pencarian);
        $this->db->from($this->_table_header);
        $hasil = $this->db->count_all_results();
        //echo "<pre>";
        //print_r($hasil); die();
        //echo "</pre>";

        //pagination limit
        $pagination['base_url']     = base_url().'Pembelian/datapembelian/load/';
        $pagination['total_rows']   =$hasil;
        $pagination['per_page']     = "3";
        $pagination['uri_segment']  = 4;
        $pagination['num_links']    = 2;

        //custom pagging configuration
        $pagination['full_tag_open']    = '<div class="pagination">';
        $pagination['full_tag_close']   = '</div>';

        $pagination['first_link']       = 'First Page';
        $pagination['first_tag_open']   = '<span class="firstlink">';
        $pagination['first_tag_close']  = '</span>';

        $pagination['last_link']        = 'Last Page';
        $pagination['last_tag_open']    = '<span class="lastlink">';
        $pagination['last_tag_close']   = '</span>';

        $pagination['next_link']        = 'Next Page';
        $pagination['next_tag_open']    = '<span class="nextlink">';
        $pagination['next_tag_close']   = '</span>';

        $pagination['prev_link']        = 'Prev Page';
        $pagination['prev_tag_open']    = '<span class="prevlink">';
        $pagination['prev_tag_close']   = '</span>';

        $pagination['cur_tag_open']     = '<span class="curlink">';
        $pagination['cur_tag_close']    = '</span>';

        $pagination['num_tag_open']     = '<span class="numlink">';
        $pagination['num_tag_close']    = '</span>';

        $this->pagination->initialize($pagination);

        $hasil_pagination   = $this->tampilDataPembelianPagination($pagination['per_page'],
        $this->uri->segment(4), $data_pencarian);

        return $hasil_pagination;
    }

public function createKodeUrut(){
    //cek kode barang terakhir
    $this->db->select('MAX(no_transaksi) as no_transaksi');
    $query  = $this->db->get($this->_table_header);
    $result = $query->row_array(); //hasil bentuk array

    $no_transaksi_terakhir = $result['no_transaksi'];
    //format BR001 = BR (label awal), 001 (nomor urut)
    $label    = "TR";
    $labelthn = substr(date('Y'),-1);
    $labelbln = date('m');
    $jam      = date('H');

    if (($jam % 2) == 0) {
        $modulus = 'A';
    }else{
        $modulus = 'B';
    }

    // $lbltahun = (int) substr($lbltahun, 1,1);
    $no_urut_lama = (int) substr($no_transaksi_terakhir, 8,2);
    $no_urut_lama ++;

    $no_urut_baru = sprintf("%03s", $no_urut_lama);
    $no_transaksi_baru = $label . $labelthn . $labelbln . $jam . $no_urut_baru;

    return $no_transaksi_baru;
    }
}
