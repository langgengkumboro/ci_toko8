<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Penjualan_models extends CI_Model
{
   //panggil nama table
    private $_table_header = "penjualan_header";
    private $_table_detail = "penjualan_detail";

    public function tampilDataPenjualan()
        {
            $query  = $this->db->query(
                "SELECT * FROM " . $this->_table_header . " WHERE flag = 1"
            );
            return $query->result();  
        }

    public function savePenjualanHeader()
        {
            $data['no_transaksi']   = $this->input->post('no_transaksi');
            $data['tanggal']        = date('Y-m-d');
            $data['pembeli']        = $this->input->post('pembeli');
            $data['flag']           = 1;

            $this->db->insert($this->_table_header, $data);
        }
     public function idTransaksiTerakhir()
        {
            $query  = $this->db->query(
                "SELECT * FROM " . $this->_table_header . " WHERE flag = 1 ORDER BY id_jual_h DESC LIMIT 0,1"
            );
            $data_id = $query->result();

            foreach ($data_id as $data) {
                $last_id = $data->id_jual_h;
            }

            return $last_id;
        }

    public function tampilDataPenjualanDetail($id)
        {
            $query  = $this->db->query(
                "SELECT A.*, B.nama_barang FROM " . $this->_table_detail . " AS A INNER JOIN barang AS B ON A.kode_barang = B.kode_barang WHERE A.flag = 1 AND A.id_jual_h = '$id'"
            );
            // $query = $this->db->query("SELECT * FROM " . $this->_table_header . " WHERE flag = 1");
            return $query->result();    
        }

    public function savePenjualanDetail($id)
        {
            $kode_barang = $this->input->post('kode_barang');
            $harga_barang       = $this->Barang_models->TampilHargaBarang($kode_barang);

            $qty    = $this->input->post('qty');
            $harga  = $this->input->post('harga');

            $data['id_jual_h'] = $id;
            $data['kode_barang']    = $kode_barang;
            $data['qty']            = $qty;
            $data['harga']          = $harga_barang;
            $data['jumlah']         = $qty * $harga_barang;
            $data['flag']           = 1;

            $this->db->insert($this->_table_detail, $data);
        }
// public function tampillaporanpembelian($tgl_awal,$tgl_akhir)
public function tampillaporanpenjualan($tgl_awal, $tgl_akhir)

    {
        $this->db->select(' ph.id_jual_h, ph.no_transaksi,ph.tanggal, COUNT(pd.kode_barang) AS total_barang, SUM(pd.qty) as total_qty, SUM(pd.jumlah) as total_pembelian '); 
        $this->db->FROM ('penjualan_header ph');
        $this->db->JOIN ('penjualan_detail pd', 'ph.id_jual_h = pd.id_jual_h');
        $this->db->where("ph.tanggal BETWEEN '$tgl_awal' AND '$tgl_akhir'");
        $this->db->GROUP_BY('ph.no_transaksi');
        $query = $this->db->get();
             return $query->result();
          //  $_config[0] =& $config;  
          // return $_config[0];
    }

public function tampilDataPenjualanPagination($perpage, $uri, $data_pencarian)
    {
        // echo "<pre>";
        // print_r($data_pencarian); die();
        // echo "</pre>";
        $this->db->select('*');
        if (!empty($data_pencarian)) {
            $this->db->like('no_transaksi', $data_pencarian);
        }
        $this->db->order_by('id_jual_h','asc');

        $get_data = $this->db->get($this->_table_header, $perpage, $uri);
        if ($get_data->num_rows() > 0) {
            return$get_data->result();
        }else{
            return null;
        }

    }

public function tombolpagination($data_pencarian)
    {
        //echo "<pre>";
        //print_r($data_pencarian); die();
        //echo "</pre>";
        //cari jmlh data berdasarkan data pencarian
        $this->db->like('no_transaksi', $data_pencarian);
        $this->db->from($this->_table_header);
        $hasil = $this->db->count_all_results();
        //echo "<pre>";
        //print_r($hasil); die();
        //echo "</pre>";

        //pagination limit
        $pagination['base_url']     = base_url().'Penjualan/datapenjualan/load/';
        $pagination['total_rows']   =$hasil;
        $pagination['per_page']     = "3";
        $pagination['uri_segment']  = 4;
        $pagination['num_links']    = 2;

        //custom pagging configuration
        $pagination['full_tag_open']    = '<div class="pagination">';
        $pagination['full_tag_close']   = '</div>';

        $pagination['first_link']       = 'First Page';
        $pagination['first_tag_open']   = '<span class="firstlink">';
        $pagination['first_tag_close']  = '</span>';

        $pagination['last_link']        = 'Last Page';
        $pagination['last_tag_open']    = '<span class="lastlink">';
        $pagination['last_tag_close']   = '</span>';

        $pagination['next_link']        = 'Next Page';
        $pagination['next_tag_open']    = '<span class="nextlink">';
        $pagination['next_tag_close']   = '</span>';

        $pagination['prev_link']        = 'Prev Page';
        $pagination['prev_tag_open']    = '<span class="prevlink">';
        $pagination['prev_tag_close']   = '</span>';

        $pagination['cur_tag_open']     = '<span class="curlink">';
        $pagination['cur_tag_close']    = '</span>';

        $pagination['num_tag_open']     = '<span class="numlink">';
        $pagination['num_tag_close']    = '</span>';

        $this->pagination->initialize($pagination);

        $hasil_pagination   = $this->tampilDataPenjualanPagination($pagination['per_page'],
        $this->uri->segment(4), $data_pencarian);

        return $hasil_pagination;
    }
public function createKodeUrut(){
    //cek kode barang terakhir
    $this->db->select('MAX(no_transaksi) as no_transaksi');
    $query  = $this->db->get($this->_table_header);
    $result = $query->row_array(); //hasil bentuk array

    $no_transaksi_terakhir = $result['no_transaksi'];
    //format BR001 = BR (label awal), 001 (nomor urut)
    $label = "PJ";
    $no_urut_lama = (int) substr($no_transaksi_terakhir, 2,3);
    $no_urut_lama ++;

    $no_urut_baru = sprintf("%03s", $no_urut_lama);
    $no_trans_baru= $label . $no_urut_baru;

    return $no_trans_baru;
    }
}
