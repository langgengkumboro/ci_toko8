<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pembelian extends CI_Controller {

    public function __construct()
            {
                parent::__construct();
                //load model terkait
                $this->load->model("Pembelian_models");
                $this->load->model("Supplier_models");
                $this->load->model("Barang_models");
                //cek sesi login
        $user_login = $this->session->userdata();
        if(count($user_login) <= 1){
            redirect("auth/index", "refresh");
        }
        $this->load->library('pdf/pdf');
        }

    public function index()
            {
                $this->datapembelian();
            }

    public function datapembelian()
            {
                // $data['data_pembelian']    = $this->Pembelian_models->tampilDataPembelian();
                // $data['data_supplier']     = $this->Supplier_models->tampilDataSupplier();
                // $data['constent']          = 'forms/data_pembelian';
             
                // $data['content']    ='forms/list_transaksi';
                // $this->load->view('Home_2', $data);
        
                if (isset($_POST['cari_data'])) {
            $data['kata_pencarian'] = $this->input->post('cari_nama');
            $this->session->set_userdata('session_pencarianPembelian', $data['kata_pencarian']);
                }else{
                    $data['kata_pencarian'] =$this->session->userdata('session_pencarianPembelian');
                }
                //  echo "<pre>";
                // print_r($this->session->userdata()); die();
                // echo "</pre>";

                $data['datapembelian']  = $this->Pembelian_models->tombolpagination($data
                    ['kata_pencarian']);

                $data['content']    = 'forms/list_transaksi';
                $this->load->view('Home_2', $data);


            }

   /* public function input_pembelian_header()
    {
        $notrans = $_POST['no_transaksi']; 
        $kdsup   = $_POST['kode_supplier']; 
        $tgl_h   = date('Y-m-d');

        $this->db->query('INSERT INTO pembelian_header (no_transaksi,kode_supplier,tanggal)
                          values("'.$notrans.'","'.$kdsup.'","'.$tgl_h.'")');

        redirect('Pembelian/inputDetail/'.$notrans);

    } */
    public function input()
            {
                 // panggil data supplier untuk kebutuhan form input
                $data['data_supplier']      = $this->Supplier_models->tampilDataSupplier();
                $data['kode_trans_baru']  = $this->Pembelian_models->createKodeUrut();
                // proses simpan ke pembelian header jika ada request form
                if (!empty($_REQUEST)) {
                    $m_pembelianheader = $this->Pembelian_models;
                    $m_pembelianheader->savePembelianHeader();
                    //panggil ID transaksi terakhir
                    $id_terakhir = $m_pembelianheader->idTransaksiTerakhir();
                //     //redirect ke halaman input pembelian detail
                    redirect("Pembelian/inputDetail/" . $id_terakhir, "refresh");
                }
                    $data['content']    ='forms/input_pembelian_header';
                    $this->load->view('Home_2', $data);
            }

     public function inputDetail($id_pembelian_header)
            {

                // panggil data barang untuk kebutuhan form input
                $data['id_header']              = $id_pembelian_header;
                $data['data_barang']            = $this->Barang_models->tampilDataBarang();
                $data['data_pembelian_detail']  = $this->Pembelian_models->tampilDataPembelianDetail($id_pembelian_header);
                
                //proses simpan ke pembelian detail jika ada request form
                if (!empty($_REQUEST)) {
                    //save detail
                    $this->Pembelian_models->savePembelianDetail($id_pembelian_header); 
                    //proses update stok
                    $kd_barang  = $this->input->post('kode_barang');
                    $qty        = $this->input->post('qty');
                    $this->Barang_models->updateStok($kd_barang, $qty);

                    redirect("Pembelian/inputDetail/" . $id_pembelian_header, "refresh");
                }
                // $validation = $this->form_validation;
                // $validation->set_rules($this->Pembelian_models->rules02());

                // if ($validation->run()){
                //    $this->Pembelian_models->savePembelianDetail($id_pembelian_header);
                //    $this->session->set_flashdata('info', '<div style="color : green">SIMPAN DATA BERHASIL HOREEE </div>');
                // redirect("Pembelian/inputDetail/" . $id_pembelian_header, "refresh");
                //     }
                $data['content']    ='forms/input_pembelian_detail';
                $this->load->view('Home_2', $data); 
            }

 public function report()  {
    
    $data['content']    ='forms/report';
    $this->load->view('Home_2', $data);    
   }
public function laporan()  
    {

                    // echo "<prev>";
                    //     print_r($this->input->post('tanggal_awal'));die();
                    // echo "</prev>";
        // if (!empty($_REQUEST)) {
                    $tgl_awal   = $this->input->post('tgl_awal');
                    $tgl_akhir   = $this->input->post('tgl_akhir');
                    $data['data_pembelian_detail']  = $this->Pembelian_models->tampillaporanpembelian($tgl_awal, $tgl_akhir);
                    $data['tgl_awal'] = $tgl_awal;
                    $data['tgl_akhir'] = $tgl_akhir;

                    $data['content']                ='forms/laporan';
                    $this->load->view('Home_2', $data);
                
                    

        // }else{
            // redirect("Pembelian/laporan/", "refresh");
    
       // }
   }

    // // $data['data_pembelian_detail'] = $this->Pembelian_models->tampillaporanpembelian();
    // // $data['content']    ='forms/laporan';
    // $this->load->view('Home_2', $data);

function cetakpdf($tgl_awal, $tgl_akhir){
        $pdf = new FPDF('l','mm','A5');
        // membuat halaman baru
        $pdf->AddPage();
        // setting jenis font yang akan digunakan
        $pdf->SetFont('Arial','B',10);
        // mencetak string 
        $pdf->Cell(187,7,'TOKO JAYA ABADI Cabang Jakut',0,1,'C');
        $pdf->SetFont('Arial','B',12);
        $pdf->Cell(190,7,'Laporan Pembelian',0,1,'C');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Cell(10,7,'',0,1);
        $pdf->SetFont('Arial','B',10,'C');
        $pdf->Cell(30,6,'ID Pembelian',1,0,'C');
        $pdf->Cell(23,6,'NO Trans',1,0,'C');
        $pdf->Cell(30,6,'Tanggal',1,0,'C');
        $pdf->Cell(30,6,'Total Barang',1,0,'C');
        $pdf->Cell(20,6,'Total QTY',1,0,'C');
        $pdf->Cell(49,6,'Jumlah Nominal Pembelian',1,1,'C');


        $pdf->SetFont('Arial','B',10);
        $no     = 0;
        $total  = 0;
        $total_keseluruhan=0;
        $laporan = $this->Pembelian_models->tampillaporanpembelian($tgl_awal, $tgl_akhir);
        


        foreach($laporan as $data){
            $no ++;
            $pdf->Cell(30,6,$data->id_pembelian_h,1,0,'C');
            $pdf->Cell(23,6,$data->no_transaksi,1,0,'C');
            $pdf->Cell(30,6,$data->tanggal,1,0,'C');
            $pdf->Cell(30,6,$data->total_barang,1,0,'C');
            $pdf->Cell(20,6,$data->total_qty,1,0,'C');
            $pdf->Cell(49,6,'Rp.'.number_format($data->total_pembelian) ,1,1,'R');
          

            // $pdf->Cell(25,6,$data->Rp. number_format($data->jumlah) 1,1); 
            $total_keseluruhan += $data->total_pembelian;
           
        }
        $pdf->SetFont('Arial','B',10);
        $pdf->Cell(153,6,'total keseluruhan',1,0,'C');
        $pdf->Cell(29,6,'Rp.'.number_format($total_keseluruhan) ,1,0,'R');
       
        $pdf->Output();
    }

} 



